#!/bin/sh

RELEASE=$1
NAMESPACE=$2
helm uninstall -n ${NAMESPACE} ${RELEASE}-runner
