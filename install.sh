#!/bin/sh

# First argument is release name, 
# second one is namespace
RELEASE=$1
NAMESPACE=$2

helm repo add gitlab https://charts.gitlab.io
helm upgrade --install --namespace ${NAMESPACE} -f values/values-$RELEASE-${NAMESPACE}.yaml $RELEASE-runner --version 0.74.0 gitlab/gitlab-runner

